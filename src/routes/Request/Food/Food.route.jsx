import React, { useState, useRef } from "react";

import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";

import { useRequestContext } from "../../../context/RequestsContext";

import { MdHealthAndSafety } from "react-icons/md";
import { AiOutlineCheckSquare, AiOutlineInfoCircle } from "react-icons/ai";

import imgPlaceholder from "../../../assets/images/img-placeholder.jpg";
import illustrationSrc from "../../../assets/images/food-ill.svg";

import "./Food.styles.scss";

const Food = () => {
  const { addNewFoodRequest } = useRequestContext();

  const [showSubmitMessage, setShowSubmitMessage] = useState(false);
  console.log(showSubmitMessage);

  const nameRef = useRef();
  const addressRef = useRef();
  const phoneRef = useRef();
  const commentRef = useRef();
  const medicinesRef = useRef();
  const deliveryRef = useRef();
  const pickupRef = useRef();
  const dateTimeRef = useRef();
  // const [basicInfo, setBasicInfo] = useState({ name: "", address: "", phone: "" });
  // const [enteredMedicines, setEnteredMedicines] = useState("");
  // const [enteredComment, setEnteredComment] = useState("");

  // const [prescriptionImgInput, setPrescriptionImgInput] = useState();
  const [identicationImgInput, setIdenticationImgInput] = useState();

  const fileReader = new FileReader();

  // const prescriptionImgChangeHandler = e => {
  //     const file = e.target.files[0];
  //     if (!file) return;

  //     fileReader.onload = e => {
  //         const { result } = e.target;
  //         setPrescriptionImgInput(result);
  //     };

  //     fileReader.readAsDataURL(file);
  // };

  const identificationImgChangeHandler = (e) => {
    const file = e.target.files[0];
    if (!file) return;

    fileReader.onload = (e) => {
      const { result } = e.target;
      setIdenticationImgInput(result);
    };

    fileReader.readAsDataURL(file);
  };

  const submitHandler = (e) => {
    e.preventDefault();

    const enteredName = nameRef.current.value;
    const enteredAddress = addressRef.current.value;
    const enteredPhone = phoneRef.current.value;
    const enteredComment = commentRef.current.value;
    const enteredMedicines = medicinesRef.current.value;
    const enteredDateTime = dateTimeRef.current.value;

    const isDeliverySelected = deliveryRef.current.checked;
    // const isPickupSelected = pickupRef.current.checked;
    const deliveryOrPickup = isDeliverySelected ? "delivery" : "pickup";

    const [date, time] = enteredDateTime.split("T");

    const request = {
      requestID: uuidv4(),
      user: {
        name: enteredName,
        address: enteredAddress,
        phone: enteredPhone,
      },
      comment: enteredComment,
      deliveryOrPickup,
      status: "review",
      identificationSrc: identicationImgInput,
      date,
      time,
      requestedFood: enteredMedicines.split(",").map((med) => med.trim()),
      assignedPersonnel: {},
    };

    addNewFoodRequest(request);
    setShowSubmitMessage(true);
  };

  const afterSubmissionMessage = (
    <div className="submission-msg">
      <AiOutlineInfoCircle />
      <p>
        We'll verify your requests, please wait for further instructions or
        updates.
      </p>
    </div>
  );

  return (
    <section className="medicine ">
      <img
        className="cover-img-food"
        src="https://img.freepik.com/free-photo/many-different-berries-form-frame-white-background_485709-54.jpg?w=826&t=st=1693037478~exp=1693038078~hmac=c3a9330192c914f1b5163f77282f37ca6fdcf5a8d6d31341154365bef60ce9a8"
        alt=""
      />
      <div className="content section-py section-px">
        <div className="left">
          <h1 className="heading-secondary">
            <MdHealthAndSafety />
            <span>Request for Food</span>
          </h1>

          {!showSubmitMessage && (
            <form onSubmit={submitHandler} className="form">
              {/* BOX 1 */}
              <div className="form__box">
                <div className="number">1</div>
                <div className="fields">
                  <div className="heading">Please fill out every fields</div>
                  <div className="inputs">
                    <div className="control">
                      <label htmlFor="name">Name: </label>
                      <input ref={nameRef} id="name" type="text" />
                    </div>
                    <div className="control">
                      <label htmlFor="address">Address: </label>
                      <input ref={addressRef} id="address" type="text" />
                    </div>
                    <div className="control">
                      <label htmlFor="phone">Phone No: </label>
                      <input ref={phoneRef} id="phone" type="tel" />
                    </div>
                  </div>
                </div>
              </div>
              {/* BOX 2 */}
              <div className="form__box">
                <div className="number">2</div>
                <div className="fields">
                  <div className="heading">
                    <p>Requirement</p>
                    <small>Please separate each name with a comma.</small>
                  </div>
                  <div className="inputs">
                    <textarea
                      ref={medicinesRef}
                      className="medicines-name"
                      rows="5"
                    ></textarea>
                  </div>
                </div>
              </div>

              {/* BOX 3 */}
              <div className="form__box">
                <div className="number">3</div>
                <div className="fields">
                  <div className="heading">
                    Add comment <small>(optional)</small>
                  </div>
                  <div className="inputs">
                    <textarea
                      ref={commentRef}
                      className="medicines-name"
                      rows="5"
                    ></textarea>
                  </div>
                </div>
              </div>
              {/* BOX 4 */}
              <div className="form__box">
                <div className="number">4</div>
                <div className="fields">
                  <div className="heading">
                    <p>
                      Identification{" "}
                      <small>
                        (<em>Birth Certificate, Goverment-Issued ID</em>)
                      </small>
                    </p>
                  </div>
                  <div className="inputs">
                    <div
                      className="img-upload-container"
                      style={{
                        backgroundImage: `url(${
                          identicationImgInput || imgPlaceholder
                        })`,
                      }}
                    >
                      <input
                        type="file"
                        onChange={identificationImgChangeHandler}
                        accept="image/png, image/jpeg"
                      />
                    </div>
                  </div>
                </div>
              </div>

              {/* BOX 5 */}
              <div className="form__box">
                <div className="number">5</div>
                <div className="fields">
                  <div className="heading">
                    <span>Select if delivery or pickup</span>
                  </div>
                  <div className="inputs">
                    <div className="radion-buttons">
                      <div>
                        <label htmlFor="delivery">Delivery</label>
                        <input
                          type="radio"
                          id="delivery"
                          name="mode"
                          value="delivery"
                          ref={deliveryRef}
                        />
                      </div>
                      <div>
                        <label htmlFor="pickup">Pickup</label>
                        <input
                          type="radio"
                          id="pickup"
                          name="mode"
                          value="pickup"
                          ref={pickupRef}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* BOX 6 */}
              <div className="form__box">
                <div className="number">6</div>
                <div className="fields">
                  <div className="heading">
                    <span>Pick a date and time</span>
                  </div>
                  <div className="inputs">
                    <input type="datetime-local" ref={dateTimeRef} />
                  </div>
                </div>
              </div>

              <div className="form__box">
                <div className="submission">
                  <div>
                    <strong>sahayatasetu</strong> is committed to protecting and
                    respecting your privacy, and we'll only use your personal
                    information to administer your account and to validate if
                    you are eligible for such requests.
                    <br />
                  </div>
                  <button type="submit" className="submit-btn">
                    Submit
                  </button>
                </div>
              </div>
            </form>
          )}
          {showSubmitMessage && afterSubmissionMessage}
        </div>
        <aside className="right-panel">
          <img className="illustration" src="https://img.freepik.com/free-vector/charity-concept-illustration_114360-2554.jpg?w=740&t=st=1693045822~exp=1693046422~hmac=9bf075ba022df9372c34d81d98e81fa5d37bb003e351e12eaa87122c92b8743d" alt="" />
          <div className="content-info">
            <div className="">
              <h3 className="heading">How it works?</h3>
              <ul>
                <li>
                  <AiOutlineCheckSquare />
                  Make your report request
                </li>
                <li>
                  <AiOutlineCheckSquare />
                  Properly provide all required information
                </li>
                <li>
                  <AiOutlineCheckSquare />
                  Wait for approval
                </li>
              </ul>
            </div>

            <div className="contact">
              <h3>Contact Us</h3>
              <Link to=""> Chat with Support</Link>
            </div>
          </div>
        </aside>
      </div>
    </section>
  );
};

export default Food;
