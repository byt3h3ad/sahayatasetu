import React, { useState, useRef } from "react";

import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";

import { useRequestContext } from "../../../context/RequestsContext";

import { MdHealthAndSafety } from "react-icons/md";
import { VscWarning } from "react-icons/vsc";
import { AiOutlineCheckSquare, AiOutlineInfoCircle } from "react-icons/ai";

import imgPlaceholder from "../../../assets/images/img-placeholder.jpg";
import illustrationSrc from "../../../assets/images/medical-prescription.gif";

import "./Mental.styles.scss";

const Medicine = () => {
    const { addNewMedicinesRequest } = useRequestContext();

    const [showSubmitMessage, setShowSubmitMessage] = useState(false);
    console.log(showSubmitMessage);

    const nameRef = useRef();
    const addressRef = useRef();
    const phoneRef = useRef();
    const commentRef = useRef();
    const medicinesRef = useRef();
    const deliveryRef = useRef();
    const pickupRef = useRef();
    const dateTimeRef = useRef();
    // const [basicInfo, setBasicInfo] = useState({ name: "", address: "", phone: "" });
    // const [enteredMedicines, setEnteredMedicines] = useState("");
    // const [enteredComment, setEnteredComment] = useState("");

    const [prescriptionImgInput, setPrescriptionImgInput] = useState();
    const [identicationImgInput, setIdenticationImgInput] = useState();

    const fileReader = new FileReader();

    const prescriptionImgChangeHandler = e => {
        const file = e.target.files[0];
        if (!file) return;

        fileReader.onload = e => {
            const { result } = e.target;
            setPrescriptionImgInput(result);
        };

        fileReader.readAsDataURL(file);
    };

    const identificationImgChangeHandler = e => {
        const file = e.target.files[0];
        if (!file) return;

        fileReader.onload = e => {
            const { result } = e.target;
            setIdenticationImgInput(result);
        };

        fileReader.readAsDataURL(file);
    };

    const submitHandler = e => {
        e.preventDefault();

        const enteredName = nameRef.current.value;
        const enteredAddress = addressRef.current.value;
        const enteredPhone = phoneRef.current.value;
        const enteredComment = commentRef.current.value;
        const enteredMedicines = medicinesRef.current.value;
        const enteredDateTime = dateTimeRef.current.value;

        const isDeliverySelected = deliveryRef.current.checked;
        // const isPickupSelected = pickupRef.current.checked;
        const deliveryOrPickup = isDeliverySelected ? "delivery" : "pickup";

        const [date, time] = enteredDateTime.split("T");

        const request = {
            requestID: uuidv4(),
            user: {
                name: enteredName,
                address: enteredAddress,
                phone: enteredPhone,
            },
            comment: enteredComment,
            deliveryOrPickup,
            status: "review",
            identificationSrc: identicationImgInput,
            prescriptionSrc: prescriptionImgInput,
            date,
            time,
            requestedMedicines: enteredMedicines.split(",").map(med => med.trim()),
            assignedPersonnel: {},
        };

        addNewMedicinesRequest(request);
        setShowSubmitMessage(true);
    };

    const afterSubmissionMessage = (
        <div className="submission-msg">
            <AiOutlineInfoCircle />
            <p>We'll verify your requests, please wait for further instructions or updates.</p>
        </div>
    );

    return (
        <section className="medicine ">
            <img
                className="cover-img"
                src="https://img.freepik.com/free-vector/adventure-background_23-2149058587.jpg?w=1060&t=st=1693046603~exp=1693047203~hmac=05ab8fcc5dfc64e58005eb192a193c6a7a8c48fb9ab4831224ff8e4d28024ff2"
                alt=""
            />
            <div className="content section-py section-px">
                <div className="left">
                    

                    <h1 className="heading-secondary">
                        <MdHealthAndSafety />
                        <span>Talk to a Professional</span>
                    </h1>

                    {!showSubmitMessage && (
                        <form onSubmit={submitHandler} className="form">
                            {/* BOX 1 */}
                            <div className="form__box">
                                <div className="number">1</div>
                                <div className="fields">
                                    <div className="heading">Fill out Details</div>
                                    <div className="inputs">
                                        <div className="control">
                                            <label htmlFor="name">Name: </label>
                                            <input ref={nameRef} id="name" type="text" />
                                        </div>
                                        <div className="control">
                                            <label htmlFor="address">Address: </label>
                                            <input ref={addressRef} id="address" type="text" />
                                        </div>
                                        <div className="control">
                                            <label htmlFor="phone">Phone No: </label>
                                            <input ref={phoneRef} id="phone" type="tel" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            {/* BOX 2 */}
                            <div className="form__box">
                                <div className="number">2</div>
                                <div className="fields">
                                    <div className="heading">
                                        Message to the professional <small>(optional)</small>
                                    </div>
                                    <div className="inputs">
                                        <textarea
                                            ref={commentRef}
                                            className="medicines-name"
                                            rows="5"
                                        ></textarea>
                                    </div>
                                </div>
                            </div>

                            {/* BOX 2 */}
                            <div className="form__box">
                                <div className="number">3</div>
                                <div className="fields">
                                    <div className="heading">
                                        Helpful Resources 
                                    </div>
                                    <div className="resources">
                                         
                                    <a href="http://www.mhresources.org/" target="_blank"><p>Read about multiple Resources</p></a>
                                    
                                    </div>
                                    <div className="resources">
                                         
                                    <a href="https://www.nimh.nih.gov/health/find-help" target="_blank"><p>NIMH Helpline </p></a>
                                    
                                    </div>
                                    <div className="resources">
                                         
                                    <a href="https://www.cdc.gov/mentalhealth/tools-resources/index.htm" target="_blank"><p>CDC Mental Health Tools and Resources</p></a>
                                    
                                    </div>
                                        </div>
                            </div>
                         

                            

                            <div className="form__box">
                                <div className="submission">
                                    <div>
                                        <strong>sahayatasetu</strong> is committed to protecting
                                        and respecting your privacy, and we'll only use your
                                        personal information to administer your account and to
                                        validate if you are eligible for such requests.
                                        <br />
                                    </div>
                                    <button type="submit" className="submit-btn">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    )}
                    {showSubmitMessage && afterSubmissionMessage}
                </div>
                <aside className="right-panel">
                    <img className="illustration" src="https://img.freepik.com/free-vector/people-connecting-jigsaw-pieces-head-together_53876-64617.jpg?w=1060&t=st=1693043763~exp=1693044363~hmac=f57bf764bb62e766aa1b99285d8b3257b033c747e418e77195fcfc17e83aa1f0" alt="" />
                    <div className="content-info">
                        <div className="">
                            <h3 className="heading">How it works?</h3>
                            <ul>
                                <li>
                                    <AiOutlineCheckSquare />
                                    Make your report request
                                </li>
                                <li>
                                    <AiOutlineCheckSquare />
                                    Properly provide all required information
                                </li>
                                <li>
                                    <AiOutlineCheckSquare />
                                    Wait for approval
                                </li>
                            </ul>
                        </div>

                        <div className="contact">
                            <h3>Contact Us</h3>
                            <Link to=""> Chat with Support</Link>
                        </div>
                    </div>
                </aside>
            </div>
        </section>
    );
};

export default Medicine;
