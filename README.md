![landing](misc/landing.jpg)

# [Sahayatasetu](https://sahayatasetu.vercel.app/)

## 💡 Inspiration 💡

The goal or aim of this app is to directly connect needy people with NGOs and small businesses who want to donate or help them by providing basic amenities like medicines, food etc. We aim to simplify the process of connecting the needy with the helpful.

## ⚙ What it does ⚙

SahayataSetu acts as a "setu" or a bridge between these NGOs and small businesses and the people in need of help.

It allows people, ideally, residents of a certain town that the app will be used, and they can ask for assistance, e.g. medicine, food, financial, and other basic needs. They will place a request and will need to provide all required documents such as prescriptions and identification (government-issued) and the people in charge will check the database monitored by government to verify if the documents are genuine.
